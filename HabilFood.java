import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class HabilFood {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double totalHarga = 0;
        StringBuilder struk = new StringBuilder();

        System.out.println("Selamat datang di Habil Food!");
        System.out.println("Menu Makanan:");
        System.out.println("1. Nasi Goreng - Rp 25000");
        System.out.println("2. Mie Goreng - Rp 22000");
        System.out.println("3. Ayam Goreng - Rp 30000");
        System.out.println("4. Es Teh - Rp 5000");
        System.out.println("5. Es Jeruk - Rp 7000");

        while (true) {
            System.out.print("Pilih nomor menu (0 untuk selesai): ");
            int pilihan = scanner.nextInt();
            if (pilihan == 0) {
                break;
            }
            String menu = "";
            double harga = 0;

            switch (pilihan) {
                case 1:
                    menu = "Nasi Goreng";
                    harga = 25000;
                    break;
                case 2:
                    menu = "Mie Goreng";
                    harga = 22000;
                    break;
                case 3:
                    menu = "Ayam Goreng";
                    harga = 30000;
                    break;
                case 4:
                    menu = "Es Teh";
                    harga = 5000;
                    break;
                case 5:
                    menu = "Es Jeruk";
                    harga = 7000;
                    break;
                default:
                    System.out.println("Nomor menu tidak valid.");
                    continue;
            }

            System.out.print("Masukkan jumlah pesanan: ");
            int jumlah = scanner.nextInt();
            double subtotal = harga * jumlah;
            totalHarga += subtotal;

            struk.append(menu).append("\t\tRp ").append(harga).append("\t").append(jumlah).append(" pcs\t\tRp ").append(subtotal).append("\n");
        }

        System.out.println("\nTotal harga: Rp " + totalHarga);

        try (BufferedWriter writer = new BufferedWriter(new FileWriter("struk_pembelian.txt"))) {
            writer.write("Habil Food\n");
            writer.write("=========================================\n");
            writer.write("Menu\t\tHarga\tJumlah\tSubtotal\n");
            writer.write("=========================================\n");
            writer.write(struk.toString());
            writer.write("=========================================\n");
            writer.write("Total Harga:\t\t\t\tRp " + totalHarga);
            System.out.println("Struk pembelian telah disimpan dalam file struk_pembelian.txt");
        } catch (IOException e) {
            System.err.println("Gagal menyimpan struk pembelian: " + e.getMessage());
        }
    }
}
